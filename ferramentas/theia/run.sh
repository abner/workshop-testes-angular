#!/bin/bash

PROJECT_ROOT_DIRECTORY=$PWD

if [ -z $PROJECT_ROOT_DIRECTORY ]; then
    echo 'Variável PROJECT_ROOT_DIRECTORY não está definida!'
    exit 1;
fi

if ! docker images ngapp_theia_ide | grep ngapp_theia_ide ; then
    docker build --build-arg UID=$UID --build-arg GID=$(id -g) $PROJECT_ROOT_DIRECTORY/ferramentas/theia -t ngapp_theia_ide:latest && \
    docker run --rm --name ngapp_theia_ide_1 -e UID=$UID -e GID=$(id -g) -it -p 3000:3000 -p 4200:4200 -p 3333:3333 -v "$PROJECT_ROOT_DIRECTORY/ng-app:/home/project:cached" ngapp_theia_ide:latest
else
    docker run --rm --name ngapp_theia_ide_1 -e UID=$UID -e GID=$(id -g) -it -p 3000:3000 -p 4200:4200 -p 3333:3333 -v "$PROJECT_ROOT_DIRECTORY/ng-app:/home/project:cached" ngapp_theia_ide:latest
fi
