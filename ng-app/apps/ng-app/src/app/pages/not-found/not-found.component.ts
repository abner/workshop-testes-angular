import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngapp-not-found',
  styleUrls: ['./not-found.component.scss'],
  templateUrl: './not-found.component.html'
})
export class NotFoundComponent {
  constructor(private router: Router) {}

  goToInicio() {
    this.router.navigate(['paginas', 'inicio']);
  }
}
