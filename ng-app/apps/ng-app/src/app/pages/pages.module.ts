import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { InicioComponent } from './inicio/inicio.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [PagesComponent, InicioComponent, NotFoundComponent],
  exports: [PagesRoutingModule],
  entryComponents: [PagesComponent],
  imports: [CommonModule, PagesRoutingModule]
})
export class PagesModule {}
