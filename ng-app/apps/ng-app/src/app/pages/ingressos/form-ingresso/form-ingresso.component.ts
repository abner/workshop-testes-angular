import { Component, OnInit } from '@angular/core';
import { Ingresso } from '../../../core/models/ingresso';
import { IngressosService } from '../../../core/ingressos/ingressos.service';
import { Observable } from 'rxjs';
import { Filme } from '../../../core/models/filme';
import { FilmesService } from '../../../core/filmes/filmes.service';

@Component({
  selector: 'form-ingresso',
  templateUrl: './form-ingresso.component.html'
})
export class FormIngressoComponent implements OnInit {
  novoIngresso: Ingresso = { filme: {} } as Ingresso;
  filmes$: Observable<Filme[]>;
  constructor(
    private ingressosService: IngressosService,
    private filmesService: FilmesService
  ) {}

  ngOnInit() {
    this.filmes$ = this.filmesService.getAll();
  }

  adicionarIngresso() {
    console.log('TESTE', this.novoIngresso);
    this.ingressosService.adicionarIngresso(this.novoIngresso);
  }
}
