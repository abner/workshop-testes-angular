import { FormIngressoComponent } from './form-ingresso.component';
import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FilmesService } from '../../../core/filmes/filmes.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { IngressosService } from '../../../core/ingressos/ingressos.service';
import { FormIngressoComponentTester } from './form-ingresso.component.cpt';

import { speculoosMatchers } from 'ngx-speculoos';

describe(FormIngressoComponent.name, () => {
  const filmes = [
    {
      id: 'vingadores_guerra_infinita',
      nome: 'Vingadores: Guerra Infinita',
      descricao:
        "Avengers: Infinity War (no Brasil, Vingadores: Guerra Infinita;[5] em Portugal, Vingadores: Guerra do Infinito[6]) é um filme estadunidense de super-herói de 2018, baseado na equipe Os Vingadores, da Marvel Comics, produzido pela Marvel Studios e distribuído pela Walt Disney Studios Motion Pictures, sendo a sequência de Marvel's The Avengers, de 2012, e Avengers: Age of Ultron, de 2015, e o décimo nono filme do Universo Cinematográfico Marvel. Dirigido por Anthony e Joe Russo e escrito por Christopher Markus e Stephen McFeely, é estrelado por Robert Downey Jr., Chris Hemsworth, Mark Ruffalo, Chris Evans, Scarlett Johansson, Benedict Cumberbatch, Don Cheadle, Tom Holland, Chadwick Boseman, Paul Bettany, Elizabeth Olsen, Anthony Mackie, Sebastian Stan, Peter Dinklage, Danai Gurira, Dave Bautista, Zoe Saldana, Josh Brolin e Chris Pratt. Em Avengers: Infinity War, os Vingadores unem forças com os Guardiões da Galáxia para enfrentar Thanos, que almeja juntar as Joias do Infinito.[7]",
      imagem: {
        arquivo: 'vingadores-guerra-infinita.jpg',
        largura: '370',
        altura: '250'
      }
    },
    {
      id: 'vingadores_ultimato',
      nome: 'vingadores: Ultimato',
      descricao:
        "Avengers: Endgame (no Brasil, Vingadores: Ultimato[1]; em Portugal, Vingadores: Endgame) é um futuro filme estadunidense de super-herói de 2019, baseado na equipe Os Vingadores, produzido pela Marvel Studios e distribuído pela Walt Disney Studios Motion Pictures, sendo a sequência de Marvel's The Avengers, de 2012, Avengers: Age of Ultron, de 2015, e Avengers: Infinity War, de 2018, e o vigésimo segundo filme do Universo Cinematográfico Marvel. Dirigido por Anthony e Joe Russo e escrito por Christopher Markus e Stephen McFelly, é estrelado pela maioria dos atores dos filmes anteriores do Universo Cinematográfico Marvel. O filme inicialmente foi anunciado como Avengers: Infinity War – Part 2. Os irmãos Russo foram contratados para dirigir em abril de 2015 e em maio, Markus e McFeely assinaram para escrever o roteiro do filme. Em julho de 2016, a Marvel removeu o título do filme, o chamando apenas de Avengers 4. As filmagens começaram em agosto de 2017, no Pinewood Atlanta Studios, no Condado de Fayette, na Geórgia, sendo filmado simultaneamente com Vingadores: Guerra Infinita. As filmagens adicionais ocorreram no centro e no metrô de Atlanta na cidade de Nova York. No dia 7 de dezembro de 2018, durante a CCXP, foi divulgado o primeiro trailer do filme com o título oficial Avengers: Endgame. A produção está programado para ser lançada nos Estados Unidos em 26 de abril de 2019,[2] nos formatos IMAX e 3D.[3] No Brasil e em Portugal, a estreia está prevista para o dia 25 de abril de 2019.",
      imagem: {
        arquivo: 'vingadores-ultimato.jpg',
        largura: '370',
        altura: '250'
      }
    }
  ];
  let cmp: ComponentFixture<FormIngressoComponent>;
  const filmesService: FilmesService = {} as FilmesService;
  const ingressosService: IngressosService = {} as IngressosService;

  beforeEach(async(() => {
    filmesService['getAll'] = jasmine
      .createSpy('getAll')
      .and.returnValue(of(filmes));

    ingressosService.adicionarIngresso = jasmine.createSpy('adicionarIngresso');

    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientModule],
      declarations: [FormIngressoComponent],
      providers: [
        { provide: FilmesService, useValue: filmesService },
        { provide: IngressosService, useValue: ingressosService }
      ]
    }).compileComponents();
  }));

  it('instancia o component', () => {
    cmp = TestBed.createComponent(FormIngressoComponent);
  });

  it('deve carregar os filmes na inicializacao', () => {
    cmp = TestBed.createComponent(FormIngressoComponent);

    cmp.componentInstance.ngOnInit();

    expect(filmesService['getAll']).toHaveBeenCalled();

    cmp.detectChanges();
  });

  describe('Após inicialização', () => {
    beforeEach(async(() => {
      cmp = TestBed.createComponent(FormIngressoComponent);
      cmp.componentInstance.ngOnInit();
      cmp.detectChanges();
    }));
    it('deve exibir os filmes para que possam ser selecionados', () => {
      const select = cmp.debugElement.query(By.css('#selectFilmes'));
      expect(select.nativeElement.options.length).toEqual(3);
    });
    it('permite adicionar ingresso', () => {
      const select = cmp.debugElement.query(By.css('#selectFilmes'))
        .nativeElement;
      // select.value = select.options[2].value;  // <-- select a new value
      select.selectedIndex = 2;
      select.dispatchEvent(new Event('change'));
      cmp.detectChanges();
      const input: HTMLInputElement = cmp.debugElement.query(
        By.css('#inputPosicao')
      ).nativeElement;
      input.value = '33';
      input.dispatchEvent(new Event('input'));
      cmp.detectChanges();
      cmp.componentInstance.adicionarIngresso();
      expect(ingressosService.adicionarIngresso).toHaveBeenCalledWith({
        filme: { id: 'vingadores_ultimato' },
        posicao: '33'
      });
    });
  });

  describe('Testes com Ngx Speculoos', () => {
    let formIngressoCPT: FormIngressoComponentTester;

    beforeEach(async(() => {
      formIngressoCPT = new FormIngressoComponentTester();
      formIngressoCPT.detectChanges();
    }));

    beforeEach(() => jest.addMatchers(speculoosMatchers));

    it('adiciona um ingresso', () => {
      formIngressoCPT.componentInstance.ngOnInit();
      formIngressoCPT.detectChanges(true);

      expect(formIngressoCPT.filmes.selectedValue).toBeNull();

      formIngressoCPT.filmes.selectValue('vingadores_guerra_infinita');
      formIngressoCPT.posicao.fillWith('33');
      formIngressoCPT.detectChanges();

      formIngressoCPT.componentInstance.adicionarIngresso();

      expect(ingressosService.adicionarIngresso).toHaveBeenCalledWith({
        filme: { id: 'vingadores_guerra_infinita' },
        posicao: '33'
      });
    });
  });
});
