import { ComponentTester } from 'ngx-speculoos';
import { FormIngressoComponent } from './form-ingresso.component';

export class FormIngressoComponentTester extends ComponentTester<
  FormIngressoComponent
> {
  constructor() {
    super(FormIngressoComponent);
  }

  get filmes() {
    return this.select('#selectFilmes');
  }

  get posicao() {
    return this.input('#inputPosicao');
  }
}
