import { Component, OnInit } from '@angular/core';
import { IngressosService } from '../../core/ingressos/ingressos.service';
import { Observable } from 'rxjs';
import { Ingresso } from '../../core/models/ingresso';
import { FilmesService } from '../../core/filmes/filmes.service';
import { Filme } from '../../core/models/filme';

@Component({
  selector: 'ngapp-ingressos',
  templateUrl: './ingressos.component.html',
  styleUrls: ['./ingressos.component.scss']
})
export class IngressosComponent implements OnInit {
  ingressos$: Observable<Ingresso[]>;
  filmes$: Observable<Filme[]>;

  constructor(
    private ingressosService: IngressosService,
    private filmesService: FilmesService
  ) {}

  ngOnInit() {
    this.ingressos$ = this.ingressosService.getTodos();
    this.filmes$ = this.filmesService.getAll();
  }
}
