import {
  async,
  ComponentFixture,
  TestBed,
  getTestBed
} from '@angular/core/testing';
import { ComponentTester } from 'ngx-speculoos';
import { IngressosComponent } from './ingressos.component';
import { DisponibilidadeIngresso } from '../../ui/disponibilidade-ingresso.pipe';
import { NgxUIModule } from '@swimlane/ngx-ui';
import { RouterTestingModule } from '@angular/router/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

describe('IngressosComponent', () => {
  let component: IngressosComponent;
  let fixture: ComponentFixture<IngressosComponent>;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  class IngressComponentTester extends ComponentTester<IngressosComponent> {
    constructor() {
      super(IngressosComponent);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxUIModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [IngressosComponent, DisponibilidadeIngresso]
    }).compileComponents();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  }));

  /* HTTP MOCK */
  /*
  // const resultadoReq = `{}`;
  // const req = httpMock.expectOne(`assets/filmes.json`);
  // expect(req.request.method).toBe("GET");
  // req.flush(svgString);
  */

  beforeEach(() => {
    fixture = TestBed.createComponent(IngressosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
