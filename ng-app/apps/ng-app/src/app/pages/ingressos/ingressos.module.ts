import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IngressosComponent } from './ingressos.component';
import { IngressosRoutingModule } from './ingressos.routing.module';
import { UIModule } from '../../ui/ui.module';
import { FormIngressoComponent } from './form-ingresso/form-ingresso.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [IngressosComponent, FormIngressoComponent],
  imports: [CommonModule, IngressosRoutingModule, UIModule, FormsModule]
})
export class IngressosModule {}
