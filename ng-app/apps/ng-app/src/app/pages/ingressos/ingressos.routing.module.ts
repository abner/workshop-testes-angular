import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IngressosComponent } from './ingressos.component';

const routes: Routes = [
  {
    path: '',
    component: IngressosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IngressosRoutingModule {}
