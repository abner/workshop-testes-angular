import { Pipe, PipeTransform } from '@angular/core';
import { Comprador } from '../core/models/comprador';

@Pipe({
  name: 'nome-comprador'
})
export class NomeComprador implements PipeTransform {
  transform(value: Comprador, ...args: any[]) {
    return value
      ? `${value.nome}: (${value.cpf ? value.cpf : 'CPF não informado'})`
      : '';
  }
}
