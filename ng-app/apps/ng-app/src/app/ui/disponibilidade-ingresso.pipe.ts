import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'disponibilidade'
})
export class DisponibilidadeIngresso implements PipeTransform {
  transform(value: boolean, ...args: any[]) {
    return value ? 'Disponível' : 'Indisponível';
  }
}
