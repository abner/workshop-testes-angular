import { NgModule } from '@angular/core';

import { NgxUIModule } from '@swimlane/ngx-ui';
import { DisponibilidadeIngresso } from './disponibilidade-ingresso.pipe';
import { NomeComprador } from './nome-comprador.pipe';

export const UI_MODULES = [NgxUIModule];

@NgModule({
  imports: [...UI_MODULES],
  declarations: [DisponibilidadeIngresso, NomeComprador],
  exports: [DisponibilidadeIngresso, NomeComprador, ...UI_MODULES]
})
export class UIModule {}
