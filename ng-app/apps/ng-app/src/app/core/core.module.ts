import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutenticacaoModule } from './autenticacao/autenticacao.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule, AutenticacaoModule]
})
export class CoreModule {}
