import { Usuario } from './usuario';

export interface UsuarioLogado {
  autenticado: boolean;
  data?: Usuario;
}
