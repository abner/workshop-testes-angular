import { Filme } from './filme';
import { Usuario } from './usuario';
import { Comprador } from './comprador';

export interface Ingresso {
  disponivel: boolean;
  posicao: number;
  valor: number;
  filme: Filme;
  compradoPor?: Comprador;
}
