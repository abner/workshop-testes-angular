import { Injectable } from '@angular/core';
import { Filme } from '../models/filme';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class FilmesService {
  constructor(private http: HttpClient) {}

  existe(filme: Filme) {
    return false;
  }

  getAll(): Observable<Filme[]> {
    return this.http.get<Filme[]>('/assets/data/filmes.json');
  }
}
