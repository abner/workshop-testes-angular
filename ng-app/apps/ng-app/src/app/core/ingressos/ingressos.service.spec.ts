import { IngressosService } from './ingressos.service';

describe('IngressosService', () => {
  describe('getTodos', () => {
    it('retorna 10 ingressos', () => {});
  });

  describe('getDisponiveis', () => {
    let ingressoService: IngressosService;

    let filmesSvc;
    let existeMocked;

    beforeEach(() => {
      existeMocked = jasmine.createSpy('existe').and.returnValue(false);
      filmesSvc = { existe: existeMocked };
      ingressoService = new IngressosService(filmesSvc);
    });

    it('retornar os 5 ingressos inicialmente disponiveis para o filme Vingadores: Guerra Infinita', done => {
      existeMocked = jasmine.createSpy('existe').and.returnValue(true);
      ingressoService['filmesSvc'] = { existe: existeMocked } as any;
      ingressoService
        .getDisponiveis({
          id: 'vingadores_guerra_infinita',
          nome: 'Vingadores: Guerra Infinita'
        })
        .subscribe(
          res => {
            expect(res.length).toEqual(5);
            expect(existeMocked).toHaveBeenCalled();
            done();
          },
          e => {
            done(e);
          }
        );
    });

    it('retornar os 5 ingressos inicialmente disponiveis para o filme Vingadores: Ultimato', () => {});
  });

  // NOTA de Aula: Parece que o desenvolvedor comeu mosca na implementação deste metódo e tem um bug lá
  describe('getVendidos', () => {
    it('retornar 0 ingressos vendidos', () => {});

    it('deve ter o resultado de ingressos vendidos incrementado apos venda', () => {});
  });
});
