import { Ingresso } from '../models/ingresso';

const filmeVingadoresGuerraInfinita = {
  id: 'vingadores_guerra_infinita',
  nome: 'Vingadores Guerra Infinita'
};

const filmeVingadoresUltimato = {
  id: 'vingadores_ultimato',
  nome: 'Vingadores - Ultimato'
};
export const ingressos: Ingresso[] = [
  {
    disponivel: true,
    posicao: 1,
    valor: 25,
    filme: filmeVingadoresGuerraInfinita
  },
  {
    disponivel: true,
    posicao: 2,
    valor: 25,
    filme: filmeVingadoresGuerraInfinita
  },
  {
    disponivel: true,
    posicao: 3,
    valor: 25,
    filme: filmeVingadoresGuerraInfinita
  },
  {
    disponivel: true,
    posicao: 4,
    valor: 25,
    filme: filmeVingadoresGuerraInfinita
  },
  {
    disponivel: true,
    posicao: 5,
    valor: 25,
    filme: filmeVingadoresGuerraInfinita
  },
  {
    disponivel: true,
    posicao: 1,
    valor: 25,
    filme: filmeVingadoresUltimato
  },
  {
    disponivel: true,
    posicao: 2,
    valor: 25,
    filme: filmeVingadoresUltimato
  },
  {
    disponivel: true,
    posicao: 3,
    valor: 25,
    filme: filmeVingadoresUltimato
  },
  {
    disponivel: true,
    posicao: 4,
    valor: 25,
    filme: filmeVingadoresUltimato
  },
  {
    disponivel: true,
    posicao: 5,
    valor: 25,
    filme: filmeVingadoresUltimato
  }
];
