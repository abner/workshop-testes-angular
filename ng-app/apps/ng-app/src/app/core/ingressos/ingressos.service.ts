import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Ingresso } from '../models/ingresso';
import { Filme } from '../models/filme';
import { Comprador } from '../models/comprador';
import { ingressos } from './ingressos';
import { FilmesService } from '../filmes/filmes.service';

@Injectable({ providedIn: 'root' })
export class IngressosService {
  ingressos: Ingresso[] = ingressos;

  // NOTA de aula -> Refatorar para colocar logica de storage em classe à parte
  constructor(private filmesSvc: FilmesService) {
    // carga inicial
    if (window && window.localStorage) {
      if (window.localStorage.getItem('ingressos')) {
        this.ingressos = JSON.parse(
          window.localStorage.getItem('ingressos')
        ) as Ingresso[];
      }
    }
  }

  adicionarIngresso(novoIngresso: Ingresso): any {
    this.ingressos.push(novoIngresso);
  }

  getTodos(): Observable<Ingresso[]> {
    return of(this.ingressos);
  }

  getDisponiveis(filme: Filme): Observable<Ingresso[]> {
    if (!this.filmesSvc.existe(filme)) {
      return throwError('FilmeNaoEncontrado');
    }
    return of(
      this.ingressos.filter(
        item => filme.id === item.filme.id && item.disponivel
      )
    );
  }

  getVendidos(filme: Filme): Observable<Ingresso[]> {
    return of(
      this.ingressos.filter(
        item => filme.id === item.filme.id && item.disponivel
      )
    );
  }

  registrarVenda(
    ingresso: Ingresso,
    comprador: Comprador
  ): Observable<boolean> {
    const indexIngresso = this.ingressos.findIndex(
      ingressoEmLista =>
        ingressoEmLista.filme.id === ingresso.filme.id &&
        ingressoEmLista.posicao === ingresso.posicao &&
        ingressoEmLista.disponivel
    );

    const resultado = false;
    if (indexIngresso >= 0) {
      this.ingressos[indexIngresso].compradoPor = comprador;
      this.ingressos[indexIngresso].disponivel = false;

      this.salvarIngressos();
    }

    return of(resultado);
  }

  cancelarVenda(ingresso: Ingresso): Observable<boolean> {
    if (ingresso.filme.id === 'vingadores-ultimato') {
      return of(false);
    }
    const indexIngresso = this.ingressos.findIndex(
      ingressoEmLista =>
        ingressoEmLista.filme.id === ingresso.filme.id &&
        ingressoEmLista.posicao === ingresso.posicao
    );
    if (indexIngresso >= 0) {
      const ingressoACancelar = this.ingressos[indexIngresso];
      ingressoACancelar.compradoPor = null;
      ingressoACancelar.disponivel = true;

      this.salvarIngressos();

      return of(true);
    } else {
      return throwError('Ingresso não encontrado!');
    }
  }

  private salvarIngressos() {
    if (window && window.localStorage) {
      window.localStorage.setItem('ingressos', JSON.stringify(this.ingressos));
    }
  }
}
