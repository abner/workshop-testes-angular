// import { TestBed, async } from '@angular/core/testing';
// import { HttpClientModule } from '@angular/common/http';
// import { UsuarioLogado } from '../models/usuario-logado';
// import { Observable } from 'rxjs';

import { AutenticacaoService } from './autenticacao.service';
import { UsuarioLogado } from '../models/usuario-logado';

describe('AutenticacaoService', () => {
  let autenticacao: AutenticacaoService;

  beforeAll(() => {});

  describe('autenticacao', () => {
    beforeEach(() => {
      autenticacao = new AutenticacaoService();
    });

    describe('autentica o usuario com sucesso', () => {
      let resultado: UsuarioLogado;

      beforeEach((
        done /* callback para sinalizacao em casos de testar codigo assincrono */
      ) => {
        autenticacao.autenticar('login', 'senha').subscribe(rs => {
          resultado = rs;
          done();
        });
      });

      it('resultado deve retornar login', () => {
        expect(resultado.data.login).toEqual('login');
      });

      it('resultado deve retornar email', () => {
        expect(resultado.data.email).toEqual('login@servico.com');
      });

      it('resultado deve retornar nome', () => {
        expect(resultado.data.nome).toEqual('Login');
      });
    });

    it('thorws "Falha na autenticação!" caso login possua menos que 5 caracteres', done => {
      autenticacao.autenticar('l', 'senha').subscribe(null, error => {
        expect(error).toEqual('Falha na autenticação!');
        done();
      });
    });
  });
});

// describe('AutenticacaoService', () => {
//   beforeEach(() =>
//     TestBed.configureTestingModule({
//       imports: [HttpClientModule]
//     })
//   );

//   let service: AutenticacaoService;
//   beforeEach(() => {
//     service = TestBed.get(AutenticacaoService);
//   });

//   it('should be created', () => {
//     expect(service).toBeTruthy();
//   });

//   describe('autentica usuario com sucesso', () => {
//     let resultado$: Observable<UsuarioLogado>;
//     beforeEach(() => {
//       resultado$ = service.autenticar('meulogin', 'minhasenha');
//     });

//     it('retorna dados sobre usuario', () => {
//       expect(resultado$).toBeDefined();
//     });

//     // NOTA DE AULA: remover o async e falhar o expect para demonstrar
//     it('retorna objeto contendo login do usuário', async(() => {
//       resultado$.subscribe(resultado => {
//         expect(resultado.data.login).toEqual('meulogin');
//       });
//     }));

//     // NOTA de aula: Refatorar para remover duplicação de código
//     it('retorna objeto contendo o email do usuário', async(() => {
//       resultado$.subscribe(resultado => {
//         expect(resultado.data.email).toEqual('meulogin@servico.com');
//       });
//     }));

//     it('retorna objeto contentdo o nome do usuário', async(() => {
//       resultado$.subscribe(resultado => {
//         expect(resultado.data.nome).toEqual('Meulogin');
//       });
//     }));
//   });

//   describe('falha na autenticacao', () => {
//     // NOTA de aula, refatorar para remover duplicação de código
//     it('retorna exceção contendo informação sobre o erro', async(() => {
//       service.autenticar('a', 'a').subscribe(null, erro => {
//         expect(erro).toBeDefined();
//       });
//     }));

//     it('retorna exceção contendo informação sobre o erro', async(() => {
//       service.autenticar('a', 'a').subscribe(null, erro => {
//         expect(erro).toEqual('Falha na autenticação!');
//       });
//     }));
//   });
// });

// REFACTORACOES

// describe('retorna objeto', () => {
//   let resultado: UsuarioLogado;

//   beforeEach(async(() => {
//     resultado$.subscribe(rs => resultado = rs);
//   }));

//   it('contendo login do usuário logado', () => {
//     expect(resultado.data.login).toEqual('meulogin');
//   });
// });
