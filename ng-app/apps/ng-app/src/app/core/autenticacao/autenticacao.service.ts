import { Injectable } from '@angular/core';
import { of, throwError, Observable } from 'rxjs';
import { UsuarioLogado } from '../models/usuario-logado';

@Injectable({
  providedIn: 'root'
})
export class AutenticacaoService {
  _usuarioLogado: UsuarioLogado = { autenticado: false, data: null };

  constructor() {}

  autenticar(login: string, senha: string): Observable<UsuarioLogado> {
    if (login.length >= 5 && senha.length >= 5) {
      this._usuarioLogado = {
        autenticado: true,
        data: {
          id: new Date().getTime().toString(),
          login,
          nome: login.charAt(0).toUpperCase() + login.substring(1),
          email: `${login}@servico.com`
        }
      };
      return of(this._usuarioLogado);
    } else {
      return throwError('Falha na autenticação!');
    }
  }

  get userLogado() {
    return this._usuarioLogado;
  }
}
