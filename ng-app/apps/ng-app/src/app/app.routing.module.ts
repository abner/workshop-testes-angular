import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages/pages.component';
import { PagesModule } from './pages/pages.module';

const routes: Routes = [
  { path: 'paginas', loadChildren: './pages/pages.module#PagesModule' },
  // { path: 'autenticacao', loadChildren: './pages/autenticacao.module#AutenticacaoModule' },
  { path: '', redirectTo: 'paginas', pathMatch: 'full' },
  { path: '**', redirectTo: 'paginas' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
